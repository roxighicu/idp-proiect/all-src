# Proiect IDP

- Roxana Stiuca, 343C4
- Alexandru Ghiculescu, 344C1

## Descriere Proiect

Proiectul presupune o aplicatie Web folosita pentru a alerta evenimente neasteptate (cutremure, explozii, incendii). Utilizatorul are acces la un Dashboard cu postari si poate, la randul sau, sa publice o postare. In cazul urgentelor, mesajul poate contine cunvatul cheie "ALERT", caz in care postarea este trimisa pe email-ul companiilor de stiri.

## Arhitectura Macro

![macro](./macroarchitecture.png)

(Aceasta este stiva aplicatiei, conform fisierului **docker-compose-deployed-kong-rmq-logging.yml**. In dezvoltare am folosit si o stiva separata, pentru Portainer.)

## Servicii Docker

Toate serviciile sunt intr-un Swarm, ce este administrat prin Portainer.

### Frontend
Este realizat in React. Am folosit Axios pentru comunicarea cu backend-ul (rutarea prin Kong).

### Backend
Python, Flask. Ofera 2 rute: /posts pentru a aduce toate postarile existente din baza de date si /new pentru a crea o postare noua. Serviciul posteaza (publish) mesajele cu keyword-ul "ALERT" in coada RabitMQ.

### Baza de date si Adminer
Pentru baza de date, am folosit imaginea mysql, cu un script de initializare a tabelelor si un volum pentru persistenta datelor.

### RabbitMQ si Worker
Worker este subscribed la coada RabbitMQ si primeste postarile de importanta ridicata prin intermediul pika. Le trimite mai departe pe mail folosing smtplib.

### Kong
Dirijeaza comunicarea serviciilor interne cu exteriorul (frontend si serviciile de logging si monitoring). Expune 2 rute: /api pentru a face cereri de GET si POST in aplicatie si /adminer pentru a folosi serviciul de Adminer. Expune porturile pentru http si https.

### Prometheus
Expune portul 9090. Este setat si pentru node-exporter si cadvisor.

### Loki si Grafana
Pe portul 3123 poate fi accesata Grafana, care poate fi configurata pentru a accesa log-uri din Loki.

## CI/CD
Am folosit imaginea de gitlab-runner pentru a asigura auto-build si auto-deploy pentru imaginile de frontend si backend.

## Pornire aplicatie

Pornire Portainer.
`docker stack deploy -c portainer/portainer-agent-stack.yml portainer`

Se adauga o stiva noua in Portainer prin fisierul **docker-compose-deployed-kong-rmq-logging.yml**.

Pot fi accesate:
- frontend: `http://localhost:3000`
- backend: `http://localhost/api`
- adminer: `http://localhost/adminer`
- Prometheus: `http://localhost:9090/metrics`, `http://localhost:9090/graph`, `http://localhost:9090/targets`
- cAdvisor: prin intermediul Prometheus sau `http://localhost:8080`
- Grafana: `http://localhost:3123` unde introduceti o noua sursa de date de la `http://loki:3100` si dupa pot fi vizualizate pe `http://localhost:3123/explore`
