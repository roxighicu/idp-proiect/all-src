create database database_schema;
use database_schema;

create table Posts (
  id integer not null primary key auto_increment,
  title varchar(40) not null,
  comment varchar(400) not null
);