import pika
import time
import smtplib
from email.mime.text import MIMEText

sender = "Word Alerts <wordalerts@mailtrap.io>"
receiver = "ProTV <protv@mailtrap.io>"


def send_mail(comment):
    message = MIMEText(comment)
    message["Subject"] = 'Subject: ALERT from Word Alerts'
    message["From"] = sender
    message["To"] = receiver

    with smtplib.SMTP("smtp.mailtrap.io", 2525) as server:
        server.login("bc9f2fb5808039", "b4c81d3e1fc817")
        server.sendmail(sender, receiver, message.as_string())


time.sleep(30)

print("Worker started")

connection = pika.BlockingConnection(
    pika.ConnectionParameters(host='rabbitmq'))
channel = connection.channel()
channel.queue_declare(queue='task_queue', durable=True)

print("Worker connected")


def callback(ch, method, properties, body):
    cmd = body.decode()
    print("Received %s" % cmd)
    send_mail(cmd)
    ch.basic_ack(delivery_tag=method.delivery_tag)


channel.basic_qos(prefetch_count=1)
channel.basic_consume(queue='task_queue', on_message_callback=callback)
channel.start_consuming()
